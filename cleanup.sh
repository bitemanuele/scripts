#!/bin/bash
echo "Stop logging services..."
/sbin/service rsyslog stop
/sbin/service auditd stop
echo ""
echo "Clean out yum."
/usr/bin/yum clean all
echo ""
echo "Force the logs to rotate & remove old logs we don’t need."
/usr/sbin/logrotate -f /etc/logrotate.conf
/bin/rm -f /var/log/*-???????? /var/log/*.gz
/bin/rm -f /var/log/dmesg.old
/bin/rm -rf /var/log/anaconda
echo ""
echo "Truncate the audit logs (and other logs we want to keep placeholders for)."
/bin/cat /dev/null > /var/log/audit/audit.log
/bin/cat /dev/null > /var/log/wtmp
/bin/cat /dev/null > /var/log/lastlog
/bin/cat /dev/null > /var/log/grubby
echo ""
echo "Remove the udev persistent device rules."
/bin/rm -f /etc/udev/rules.d/70*
echo ""
echo "Remove the traces of the template MAC address and UUIDs."
sed -i '/HWADDR\|UUID/ d' /etc/sysconfig/network-scripts/ifcfg-eth0
echo ""
echo "Clean /tmp out."
/bin/rm -rf /tmp/*
/bin/rm -rf /var/tmp/*
echo ""
echo "Remove the SSH host keys."
/bin/rm -f /etc/ssh/*key*
echo ""
echo "Remove the root user’s shell history."
/bin/rm -f ~root/.bash_history
unset HISTFILE
echo ""
echo "Remove the root user’s SSH history & other cruft."
/bin/rm -rf ~root/.ssh/
/bin/rm -f ~root/anaconda-ks.cfg
echo ""
echo "PowerOff"
shutdown -h now
