#!/bin/bash
# chkconfig: 2345 20 80
# description: Jira WebHook Services

# Source function library.
#. /etc/init.d/functions
wspath=/home/vagrant/jira_hook_receiver/
rackpath=/usr/local/rvm/gems/ruby-2.2.4/bin
rackid=$(ps aux | grep -v grep | grep '/usr/local/rvm/gems/ruby-2.2.4/bin/rackup' | awk '{print $2}')

start() {
    cd $wspath; $rackpath/rackup -p 3000 & # -o 192.168.56.1 
}

stop() {
   kill -9 $rackid
}

case "$1" in 
    start)
        echo "starting rackup.."
       start
        echo "done."
       ;;
    stop)
        ps -ef | grep -v grep | grep -v "rackup.sh status" | grep $rackpath/rackup > /dev/null
        if [ $? -eq 0 ]; then
         echo "stopping rackup.."
        stop
        echo "done."
         else
         echo "rackup not running."
       fi
       ;;
    restart)
        echo "restaring rackup.."
       stop &
       start &
        echo "done."
       ;;
    status)
        ps -ef | grep -v grep | grep -v "rackup.sh status" | grep $rackpath/rackup > /dev/null
          if [ $? -eq 0 ]; then
          echo "rackup is running -- Process ID: $rackid"
        else
          echo "rackup not running"
       fi
       ;;
    *)
       echo "Usage: $0 {start|stop|status|restart}"
esac

exit 0 
